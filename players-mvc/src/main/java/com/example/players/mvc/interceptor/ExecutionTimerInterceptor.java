/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.mvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author emile
 */
@Service
public class ExecutionTimerInterceptor implements HandlerInterceptor {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionTimerInterceptor.class);
    private long preHandleTime;
    private long postHandleTime;
    
    @Override
    public boolean preHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o) throws Exception {
        preHandleTime = System.currentTimeMillis();
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, ModelAndView mav) throws Exception {
        postHandleTime = System.currentTimeMillis();
    }

    @Override
    public void afterCompletion(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, Exception excptn) throws Exception {
        long afterCompletionTime = System.currentTimeMillis();
        LOGGER.info(ExecutionTimerInterceptor.formatLogMessage(postHandleTime - preHandleTime, afterCompletionTime - preHandleTime));
    }
    
    private static String formatLogMessage(long handlerExecutionTime, long totalExecutionTime) {
        return String.format("{handlerExecutionTime=%d, totalExecutionTime=%d}", handlerExecutionTime, totalExecutionTime);
    }
}
