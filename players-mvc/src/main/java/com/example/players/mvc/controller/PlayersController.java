/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.mvc.controller;

import com.example.players.shared.dto.PlayersRankingDto;
import com.example.players.shared.service.PlayersService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author emile
 */
@Controller
@RequestMapping("players")
public class PlayersController {
    
    @Autowired
    private PlayersService playersService;
    
    @RequestMapping("rankings")
    public String ranking(Model model) {
        List<PlayersRankingDto> playersList = playersService.getAllPlayerRankings();
        playersList.sort((p1, p2) -> p1.getRanking().compareTo(p2.getRanking()));
        model.addAttribute("playersList", playersList);
        return "rankings";
    }
    
    @RequestMapping("statistics")
    public String statistics(@RequestParam("playerId") int playerId, @RequestParam("seasonId") int seasonId, 
            @RequestParam("tournamentId") int tournamentId, Model model) {
        model.addAttribute("player", playersService.getPlayerStatistics(playerId, seasonId, tournamentId));
        return "statistics";
    }
    
    @RequestMapping("evictPlayersFromCache")
    public String evictPlayerFromCache(@RequestParam("playerId") int playerId, @RequestParam("seasonId") int seasonId,
            @RequestParam("tournamentId") int tournamentId, Model model) {
        playersService.evictPlayersFromCache(playerId, seasonId, tournamentId);
        model.addAttribute("playerId", playerId);
        model.addAttribute("seasonId", seasonId);
        model.addAttribute("tournamentId", tournamentId);
        return "evictPlayersFromCache";
    }
}
