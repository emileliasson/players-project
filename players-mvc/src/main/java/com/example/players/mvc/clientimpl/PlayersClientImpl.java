/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.mvc.clientimpl;

import com.example.players.shared.dto.PlayersRankingDto;
import com.example.players.shared.dto.PlayersStatisticsDto;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import com.example.players.shared.service.PlayersService;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author emile
 */
@Service
public class PlayersClientImpl implements PlayersService {
    
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<PlayersRankingDto> getAllPlayerRankings() {
        return Arrays.asList(restTemplate
                .getForEntity("http://localhost:8080/players-rs/players/rankings", PlayersRankingDto[].class)
                .getBody());
    }

    @Override
    public PlayersStatisticsDto getPlayerStatistics(int playerId, int seasonId, int tournamentId) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString("http://localhost:8080/players-rs/players/statistics")
                .queryParam("playerId", playerId)
                .queryParam("seasonId", seasonId)
                .queryParam("tournamentId", tournamentId);
        return restTemplate
                .getForEntity(uriBuilder.toUriString(), PlayersStatisticsDto.class)
                .getBody();
    }

    @Override
    public void evictPlayersFromCache(int playerId, int seasonId, int tournamentId) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString("http://localhost:8080/players-rs/players/evictPlayersFromCache")
                .queryParam("playerId", playerId)
                .queryParam("seasonId", seasonId)
                .queryParam("tournamentId", tournamentId);
        restTemplate.getForEntity(uriBuilder.toUriString(), Void.class);
    }
    
}
