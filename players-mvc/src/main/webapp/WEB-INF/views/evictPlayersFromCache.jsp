<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cache Evict</title>
    </head>
    <body>
        <div align="center">
            <h1>Cache Eviction</h1>
            <p>
                Cache containing player rankings evicted all entries.
                Url /players/rankings will fetch all entries from the database instead of the cache.
            </p>
            <c:set var="playerId" value="${playerId}"/>
            <c:set var="seasonId" value="${seasonId}"/>
            <c:set var="tournamentId" value="${tournamentId}"/>
            <p>
                Cache containing player statistics evicted the player with the following primary key:
                PlayerId=<c:out value="${playerId}"/>
                SeasonId=<c:out value="${seasonId}"/>
                TournamentId=<c:out value="${tournamentId}"/>
                
                Url /players/statistics?<c:out value="playerId=${playerId}&seasonId=${seasonId}&tournamentId=${tournamentId}"/> 
                will fetch the entry from the database instead of the cache.
            </p>
        </div>
    </body>
</html>
