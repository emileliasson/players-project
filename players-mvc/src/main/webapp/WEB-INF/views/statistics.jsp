<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Statistics</title>
    </head>
    <body>
        <div align="center">
            <table border="1" cellPadding="5">
                <caption><h1>Player Statistics</h1></caption>
                <tr>
                    <th>Rank</th>
                    <th>Is Active</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Height</th>
                    <th>Weight</th>
                    <th>Tournament</th>
                    <th>Season</th>
                    <th>Team</th>
                    <th>Positions</th>
                    <th>Minutes Played</th>
                    <th>Goals</th>
                    <th>Assists</th>
                    <th>Yellow Cards</th>
                    <th>Red Cards</th>
                    <th>Shots</th>
                    <th>Man of the Match</th>
                    <th>Pass success</th>
                </tr>
                <c:set var="player" value="${player}"/>
                <tr>
                    <td><c:out value="${player.ranking}"/></td>
                    <td><c:out value="${player.isActive}"/></td>
                    <td><c:out value="${player.name}"/></td>
                    <td><c:out value="${player.age}"/></td>
                    <td><c:out value="${player.height}"/></td>
                    <td><c:out value="${player.weight}"/></td>
                    <td><c:out value="${player.tournamentName}"/></td>
                    <td><c:out value="${player.seasonName}"/></td>
                    <td><c:out value="${player.teamName}"/></td>
                    <td><c:out value="${player.playedPositions}"/></td>
                    <td><c:out value="${player.minsPlayed}"/></td>
                    <td><c:out value="${player.goal}"/></td>
                    <td><c:out value="${player.assistTotal}"/></td>
                    <td><c:out value="${player.yellowCard}"/></td>
                    <td><c:out value="${player.redCard}"/></td>
                    <td><c:out value="${player.shotsPerGame}"/></td>
                    <td><c:out value="${player.manOfTheMatch}"/></td>
                    <td><c:out value="${player.passSuccess}"/></td>
                </tr>
            </table>
        </div>
    </body>
</html>
