<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
        <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
        <div align="center">
            <h1>Home</h1>
            <li>
                <a href="${contextPath}/players-mvc/players/rankings">
                    <h3>Go to Player Rankings</h3>
                </a>
            </li>
        </div>
    </body>
</html>
