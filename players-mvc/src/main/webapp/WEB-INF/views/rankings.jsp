<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Rankings</title>
    </head>
    <body>
        <div align="center">
            <h1>Player Rankings</h1>
            <table border="1" cellPadding="5">
                <caption>Click the Rank of the player whoms statistics you want to see.</caption>
                <tr>
                    <th>Rank</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Season</th>
                    <th>Tournament</th>
                </tr>
                <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
                <c:forEach var="player" items="${playersList}">
                    <tr>
                        <td>
                            <a href="${contextPath}/players/statistics?playerId=${player.playerId}&seasonId=${player.seasonId}&tournamentId=${player.tournamentId}">
                                <c:out value="${player.ranking}"/>
                            </a>
                        </td>
                        <td><c:out value="${player.lastName}"/></td>
                        <td><c:out value="${player.firstName}"/></td>
                        <td><c:out value="${player.seasonName}"/></td>
                        <td><c:out value="${player.tournamentName}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
