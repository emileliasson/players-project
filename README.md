ASSUMPTIONS
-----------
Database: MySQL

Schema name: players_schema

Table name: players (import from players.csv)

Location: 127.0.0.1:3306 (localhost)

Database user: devuser/devpass

All projects are run on the same machine.

PROJECTS
--------
players-build is the parent project that holds common dependencies. 
It helps the other project reference common code.

players-rs is working towards the database.
It return DTOs as JSONs and performs caching of DTOs.
It uses JNDI-lookup to find the datasource.

players-mvc perfrom rest calls to the players-rs application and displays the result in a web ui.

players-shared holds common code like DTOs.

CACHING
-------
http://localhost:8080/players-rs/players/rankings will return PlayersRankingDto as JSON.
If done through the web ui, they will be listed in a table.
All these DTOs will be cached in a separate cache.

http://localhost:8080/players-rs/players/statistics?playerId=73078&seasonId=4279&tournamentId=22 will return a PlayersStatisticsDto.
If done through the web ui, the statistics will be listed in a table. Click the rank of the player whoms statistics you want to see.
This DTO is stored in a cache for player statistics.

http://localhost:8080/players-rs/players/evictPlayersFromCache?playerId=73078&seasonId=4279&tournamentId=22 returns nothing.
This is just to show that the cache is working as intended.
It will evict all entries from the cache holding the player rankings.
It will also evict the player statistic entries from the player statistics cache that matches the playerId+seasonId+tournamentId.

Check the log:
The log will show which methods are being called and in what order.
If only the Controller is called, then the cache had what you were lookig for.
If the Service and the DAO is used, then the cache didnt have what we were looking for, or it was empty.


BUILDING
--------
Import all projects into your NetBeans IDE (I use NetBeans IDE 8.2 with Tomcat 9 for my webserver)

Perform a Clean and Build on the players-build project

Run players-rs

Run players-mvc (for a web ui)