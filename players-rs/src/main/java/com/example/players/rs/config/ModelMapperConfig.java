/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.config;

import com.example.players.rs.entity.Players;
import com.example.players.shared.dto.PlayersRankingDto;
import com.example.players.shared.dto.PlayersStatisticsDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author emile
 */
@Configuration
public class ModelMapperConfig {
    
    @Bean("playersRankingDtoModelMapper")
    public ModelMapper playersRankingDtoModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addMappings(new PropertyMap<Players, PlayersRankingDto>() {
            @Override
            protected void configure() {
                map().setPlayerId(source.getPlayersPK().getPlayerId());
                map().setSeasonId(source.getPlayersPK().getSeasonId());
                map().setTournamentId(source.getPlayersPK().getTournamentId());
            }
        });
        return modelMapper;
    }
    
    @Bean("noConfigModelMapper")
    public ModelMapper noConfigModelMapper() {
        return new ModelMapper();
    }
    
}
