/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author emile
 */
@Embeddable
public class PlayersPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "seasonId")
    private int seasonId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tournamentId")
    private int tournamentId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "playerId")
    private int playerId;

    public PlayersPK() {
    }

    public PlayersPK(int seasonId, int tournamentId, int playerId) {
        this.seasonId = seasonId;
        this.tournamentId = tournamentId;
        this.playerId = playerId;
    }

    public int getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) seasonId;
        hash += (int) tournamentId;
        hash += (int) playerId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlayersPK)) {
            return false;
        }
        PlayersPK other = (PlayersPK) object;
        if (this.seasonId != other.seasonId) {
            return false;
        }
        if (this.tournamentId != other.tournamentId) {
            return false;
        }
        if (this.playerId != other.playerId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.mavenproject1.enity.PlayersPK[ seasonId=" + seasonId + ", tournamentId=" + tournamentId + ", playerId=" + playerId + " ]";
    }
    
}
