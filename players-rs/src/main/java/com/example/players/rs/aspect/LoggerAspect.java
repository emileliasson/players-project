/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.aspect;

import java.util.Arrays;
import java.util.List;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 *
 * @author emile
 */
@EnableAspectJAutoProxy
@Aspect
@Component
public class LoggerAspect {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerAspect.class);
    
    @Pointcut("execution(* com.example.players.rs.controller.*.*(..))")
    private void controller() {}
    
    @Pointcut("execution(* com.example.players.rs.serviceimpl.*.*(..))")
    private void service() {}
    
    @Pointcut("execution(* com.example.players.rs.daoimpl.*.*(..))")
    private void dao() {}
    
    @Pointcut("controller() || service() || dao()")
    private void appFlow() {}
    
    @Before("appFlow()")
    public void before(JoinPoint jp) {
        LOGGER.info(new AppFlowLogItem(jp).toString());
    }
    
    public static class AppFlowLogItem {
        private final String signature;
        private final List<Object> args;
        
        public AppFlowLogItem(JoinPoint jp) {
            signature = jp.getSignature().toShortString();
            args = Arrays.asList(jp.getArgs());
        }

        @Override
        public String toString() {
            return "AppFlowLogItem{" + "signature=" + signature + ", args=" + args + '}';
        }
        
    }
}
