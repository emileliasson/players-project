/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.players.shared.dto.PlayersRankingDto;
import com.example.players.shared.dto.PlayersStatisticsDto;
import com.example.players.shared.service.PlayersService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author emile
 */
@RestController
@RequestMapping("players")
public class PlayersController {
    
    @Autowired
    private PlayersService playersService;
    
    @RequestMapping("rankings")
    public @ResponseBody List<PlayersRankingDto> rankings() {
        return playersService.getAllPlayerRankings();
    }
    
    @RequestMapping("statistics")
    public @ResponseBody PlayersStatisticsDto statistics(@RequestParam("playerId") int playerId,
            @RequestParam("seasonId") int seasonId, @RequestParam("tournamentId") int tournamentId) {
        return playersService.getPlayerStatistics(playerId, seasonId, tournamentId);
    }
    
    @RequestMapping("evictPlayersFromCache")
    public void evictPlayerFromCache(@RequestParam("playerId") int playerId,
            @RequestParam("seasonId") int seasonId, @RequestParam("tournamentId") int tournamentId) {
        playersService.evictPlayersFromCache(playerId, seasonId, tournamentId);
    }
}
