/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author emile
 */
@Entity
@Table(name = "players")
@NamedQueries({
    @NamedQuery(name = "Players.findAll", query = "SELECT p FROM Players p")})
public class Players implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PlayersPK playersPK;
    @Column(name = "ranking")
    private Integer ranking;
    @Lob
    @Size(max = 65535)
    @Column(name = "seasonName")
    private String seasonName;
    @Column(name = "tournamentRegionId")
    private Integer tournamentRegionId;
    @Lob
    @Size(max = 65535)
    @Column(name = "tournamentRegionCode")
    private String tournamentRegionCode;
    @Lob
    @Size(max = 65535)
    @Column(name = "regionCode")
    private String regionCode;
    @Lob
    @Size(max = 65535)
    @Column(name = "tournamentName")
    private String tournamentName;
    @Lob
    @Size(max = 65535)
    @Column(name = "tournamentShortName")
    private String tournamentShortName;
    @Lob
    @Size(max = 65535)
    @Column(name = "firstName")
    private String firstName;
    @Lob
    @Size(max = 65535)
    @Column(name = "lastName")
    private String lastName;
    @Lob
    @Size(max = 65535)
    @Column(name = "isActive")
    private String isActive;
    @Lob
    @Size(max = 65535)
    @Column(name = "isOpta")
    private String isOpta;
    @Column(name = "teamId")
    private Integer teamId;
    @Lob
    @Size(max = 65535)
    @Column(name = "teamName")
    private String teamName;
    @Lob
    @Size(max = 65535)
    @Column(name = "playedPositions")
    private String playedPositions;
    @Column(name = "age")
    private Integer age;
    @Column(name = "height")
    private Integer height;
    @Column(name = "weight")
    private Integer weight;
    @Lob
    @Size(max = 65535)
    @Column(name = "positionText")
    private String positionText;
    @Column(name = "apps")
    private Integer apps;
    @Column(name = "subOn")
    private Integer subOn;
    @Column(name = "minsPlayed")
    private Integer minsPlayed;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "rating")
    private Double rating;
    @Column(name = "goal")
    private Integer goal;
    @Column(name = "assistTotal")
    private Integer assistTotal;
    @Column(name = "yellowCard")
    private Integer yellowCard;
    @Column(name = "redCard")
    private Integer redCard;
    @Column(name = "shotsPerGame")
    private Double shotsPerGame;
    @Column(name = "aerialWonPerGame")
    private Double aerialWonPerGame;
    @Column(name = "manOfTheMatch")
    private Integer manOfTheMatch;
    @Lob
    @Size(max = 65535)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "isManOfTheMatch")
    private String isManOfTheMatch;
    @Lob
    @Size(max = 65535)
    @Column(name = "playedPositionsShort")
    private String playedPositionsShort;
    @Column(name = "passSuccess")
    private Double passSuccess;

    public Players() {
    }

    public Players(PlayersPK playersPK) {
        this.playersPK = playersPK;
    }

    public Players(int seasonId, int tournamentId, int playerId) {
        this.playersPK = new PlayersPK(seasonId, tournamentId, playerId);
    }

    public PlayersPK getPlayersPK() {
        return playersPK;
    }

    public void setPlayersPK(PlayersPK playersPK) {
        this.playersPK = playersPK;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public Integer getTournamentRegionId() {
        return tournamentRegionId;
    }

    public void setTournamentRegionId(Integer tournamentRegionId) {
        this.tournamentRegionId = tournamentRegionId;
    }

    public String getTournamentRegionCode() {
        return tournamentRegionCode;
    }

    public void setTournamentRegionCode(String tournamentRegionCode) {
        this.tournamentRegionCode = tournamentRegionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getTournamentShortName() {
        return tournamentShortName;
    }

    public void setTournamentShortName(String tournamentShortName) {
        this.tournamentShortName = tournamentShortName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsOpta() {
        return isOpta;
    }

    public void setIsOpta(String isOpta) {
        this.isOpta = isOpta;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getPlayedPositions() {
        return playedPositions;
    }

    public void setPlayedPositions(String playedPositions) {
        this.playedPositions = playedPositions;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    public Integer getApps() {
        return apps;
    }

    public void setApps(Integer apps) {
        this.apps = apps;
    }

    public Integer getSubOn() {
        return subOn;
    }

    public void setSubOn(Integer subOn) {
        this.subOn = subOn;
    }

    public Integer getMinsPlayed() {
        return minsPlayed;
    }

    public void setMinsPlayed(Integer minsPlayed) {
        this.minsPlayed = minsPlayed;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public Integer getAssistTotal() {
        return assistTotal;
    }

    public void setAssistTotal(Integer assistTotal) {
        this.assistTotal = assistTotal;
    }

    public Integer getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(Integer yellowCard) {
        this.yellowCard = yellowCard;
    }

    public Integer getRedCard() {
        return redCard;
    }

    public void setRedCard(Integer redCard) {
        this.redCard = redCard;
    }

    public Double getShotsPerGame() {
        return shotsPerGame;
    }

    public void setShotsPerGame(Double shotsPerGame) {
        this.shotsPerGame = shotsPerGame;
    }

    public Double getAerialWonPerGame() {
        return aerialWonPerGame;
    }

    public void setAerialWonPerGame(Double aerialWonPerGame) {
        this.aerialWonPerGame = aerialWonPerGame;
    }

    public Integer getManOfTheMatch() {
        return manOfTheMatch;
    }

    public void setManOfTheMatch(Integer manOfTheMatch) {
        this.manOfTheMatch = manOfTheMatch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsManOfTheMatch() {
        return isManOfTheMatch;
    }

    public void setIsManOfTheMatch(String isManOfTheMatch) {
        this.isManOfTheMatch = isManOfTheMatch;
    }

    public String getPlayedPositionsShort() {
        return playedPositionsShort;
    }

    public void setPlayedPositionsShort(String playedPositionsShort) {
        this.playedPositionsShort = playedPositionsShort;
    }

    public Double getPassSuccess() {
        return passSuccess;
    }

    public void setPassSuccess(Double passSuccess) {
        this.passSuccess = passSuccess;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (playersPK != null ? playersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Players)) {
            return false;
        }
        Players other = (Players) object;
        if ((this.playersPK == null && other.playersPK != null) || (this.playersPK != null && !this.playersPK.equals(other.playersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.mavenproject1.enity.Players[ playersPK=" + playersPK + " ]";
    }
    
}
