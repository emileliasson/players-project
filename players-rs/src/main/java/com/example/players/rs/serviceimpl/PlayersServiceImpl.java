/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.serviceimpl;

import com.example.players.rs.dao.PlayersDao;
import com.example.players.shared.dto.PlayersRankingDto;
import com.example.players.shared.dto.PlayersStatisticsDto;
import com.example.players.shared.service.PlayersService;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.annotations.Cache;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

/**
 *
 * @author emile
 */
@Service
public class PlayersServiceImpl implements PlayersService {
    
    @Autowired
    private PlayersDao playersDao;
    
    @Autowired
    @Qualifier("playersRankingDtoModelMapper")
    private ModelMapper playersRankingDtoModelMapper;
    
    @Autowired
    @Qualifier("noConfigModelMapper")
    private ModelMapper noConfigModelMapper;

    @Override
    @Cacheable("rankings")
    public List<PlayersRankingDto> getAllPlayerRankings() {
        return playersDao
                .getAllPlayers()
                .stream()
                .map(players -> playersRankingDtoModelMapper.map(players, PlayersRankingDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(value="statistics", key="#playerId+#seasonId+#tournamentId")
    public PlayersStatisticsDto getPlayerStatistics(int playerId, int seasonId, int tournamentId) {
        return noConfigModelMapper
                .map(playersDao.getPlayers(playerId, seasonId, tournamentId), PlayersStatisticsDto.class);
    }

    @Override
    @Caching(evict={
        @CacheEvict(value="rankings", allEntries=true),
        @CacheEvict(value="statistics", key="#playerId+#seasonId+#tournamentId")
    })
    public void evictPlayersFromCache(int playerId, int seasonId, int tournamentId) {}
    
}
