/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.rs.daoimpl;

import com.example.players.rs.entity.Players;
import com.example.players.rs.entity.PlayersPK;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.example.players.rs.dao.PlayersDao;

/**
 *
 * @author emile
 */
@Repository
public class PlayersDaoImpl implements PlayersDao {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional
    public List<Players> getAllPlayers() {
        return sessionFactory
                .getCurrentSession()
                .createNamedQuery("Players.findAll", Players.class)
                .getResultList();
    }

    @Override
    @Transactional
    public Players getPlayers(int playerId, int seasonId, int tournamentId) {
        return sessionFactory
                .getCurrentSession()
                .find(Players.class, new PlayersPK(seasonId, tournamentId, playerId));
    }
}
