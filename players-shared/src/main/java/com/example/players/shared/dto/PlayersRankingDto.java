/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.shared.dto;

/**
 *
 * @author emile
 */
public class PlayersRankingDto {
    private int playerId;
    private Integer ranking;
    private String lastName;
    private String firstName;
    private int tournamentId;
    private String tournamentName;
    private int seasonId;
    private String seasonName;

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public int getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    @Override
    public String toString() {
        return "PlayerRankingDto{" + "playerId=" + playerId + ", ranking=" + ranking + ", lastName=" + lastName + ", firstName=" + firstName + ", tournamentId=" + tournamentId + ", tournamentName=" + tournamentName + ", seasonId=" + seasonId + ", seasonName=" + seasonName + '}';
    }
    
}
