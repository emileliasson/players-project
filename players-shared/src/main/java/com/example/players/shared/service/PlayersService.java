/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.shared.service;

import com.example.players.shared.dto.PlayersRankingDto;
import com.example.players.shared.dto.PlayersStatisticsDto;
import java.util.List;

/**
 *
 * @author emile
 */
public interface PlayersService {
    List<PlayersRankingDto> getAllPlayerRankings();
    PlayersStatisticsDto getPlayerStatistics(int playerId, int seasonId, int tournamentId);
    
    void evictPlayersFromCache(int playerId, int seasonId, int tournamentId);
}
