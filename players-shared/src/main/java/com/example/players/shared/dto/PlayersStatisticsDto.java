/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.players.shared.dto;

/**
 *
 * @author emile
 */
public class PlayersStatisticsDto {
    private Integer ranking;
    private String isActive;
    private String name;
    private Integer age;
    private Integer height;
    private Integer weight;
    private String tournamentName;
    private String seasonName;
    private String teamName;
    private String playedPositions;
    private Integer minsPlayed;
    private Integer goal;
    private Integer assistTotal;
    private Integer yellowCard;
    private Integer redCard;
    private Double shotsPerGame;
    private Integer manOfTheMatch;
    private Double passSuccess;

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getPlayedPositions() {
        return playedPositions;
    }

    public void setPlayedPositions(String playedPositions) {
        this.playedPositions = playedPositions;
    }

    public Integer getMinsPlayed() {
        return minsPlayed;
    }

    public void setMinsPlayed(Integer minsPlayed) {
        this.minsPlayed = minsPlayed;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public Integer getAssistTotal() {
        return assistTotal;
    }

    public void setAssistTotal(Integer assistTotal) {
        this.assistTotal = assistTotal;
    }

    public Integer getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(Integer yellowCard) {
        this.yellowCard = yellowCard;
    }

    public Integer getRedCard() {
        return redCard;
    }

    public void setRedCard(Integer redCard) {
        this.redCard = redCard;
    }

    public Double getShotsPerGame() {
        return shotsPerGame;
    }

    public void setShotsPerGame(Double shotsPerGame) {
        this.shotsPerGame = shotsPerGame;
    }

    public Integer getManOfTheMatch() {
        return manOfTheMatch;
    }

    public void setManOfTheMatch(Integer manOfTheMatch) {
        this.manOfTheMatch = manOfTheMatch;
    }

    public Double getPassSuccess() {
        return passSuccess;
    }

    public void setPassSuccess(Double passSuccess) {
        this.passSuccess = passSuccess;
    }
    
    
}
